<?php
// В чем отличия абстрактного класса от интерфейса?
// Астрактный - Класс может быть унаследован только от одного абстрактного класса.
// Может иметь методы и данные.
// Методы могут как иметь, так и не иметь реализацию. Наследник может реализовать данный метод, а может и не реализовывать, но если метод объявлен как абстрактный, то наследник обязан иметь реализацию этого метода.
// Могут иметь статичные методы (static).
// Может иметь конструктор для потомков
// Абстрактный класс — это класс, у которого не реализован один или больше методов (некоторые языки требуют такие методы помечать специальными ключевыми словами).
// Модификаторы доступа для методов и данных класса могут быть: public, protected, private. Абстрактные методы могут быть только public либо protected.

// Интерфейс - Класс может иметь реализацию(implements) нескольких интерфейсов
// Может иметь только методы без реализации. Модификаторы доступа только — public!
// Использование protected либо private для методов приведет к ошибке.
// Могут иметь статичные методы (static).
// Интерфейс — это абстрактный класс, у которого ни один метод не реализован, все они публичные и нет переменных класса.
// Интерфейс нужен обычно когда описывается только интерфейс (тавтология). Например, один класс хочет дать другому возможность доступа к некоторым своим методам, но не хочет себя «раскрывать». Поэтому он просто реализует интерфейс.

// Астрактный

abstract class Dog{
    abstract public function getColorDog();
    public function printColorDog(){
        return 'Собака была черная, ' . $this->getColorDog();
    }
}


class NewClassDog extends Dog{
  
    public function __construct($color)
    {
        $this->color = $color;
    }
    public function getColorDog()
    {
        return  $this->color;
    }
}

$object1 = new NewClassDog('Возможно cобака была рыжая');

echo '<br>';
echo $object1->printColorDog();
echo '<br>';

// ________________________________________

abstract class Person{
    public $name;
    public $age;

    abstract public function getNane();
    public function writeName(){
return 'Он Филлип ' . $this ->getNane();
    }
    public function __construct($name, $age){
        $this ->name = $name;
        $this ->age = $age;
    }
}


class People extends Person{
    public $sernume;
    public function __construct($sernume){
        $this ->sernume = $sernume;
    }
    public function getNane(){
        return $this -> sernume;
    }
}

$result3 = new People('Моррис');
echo $result3-> writeName();

echo '<hr>';
// _____________________________________________

abstract class User
	{
		private $name;
		
		public function getName()
		{
			return $this->name;
		}
		
		public function setName($name)
		{
			$this->name = $name;
		}
		
		abstract public function upSalary($sum);
	}
	
	class Teacher extends User{
		private $salary;
		
		public function getSalary()
		{
			return 'Зарплата:' . $this->salary . 'UAH';
		}
		
		public function setSalary($salary)
		{
			$this->salary = $salary;
		}
		
		// Напишем реализацию метода:
		public function upSalary($sum){
			$this->salary = $this->salary + $sum;
	}
    }

   $result3 = new Teacher();
   $result3 -> setName('Viki'); 
   $result3 -> setSalary(15400); 
   $result3 -> upSalary(1200); 
   echo $result3->getSalary(); 
   
   echo '<hr>';	

//ИНТЕРФЕЙСЫ

interface Cats{
    public function color();
}

interface Raccoon extends Cats{
    public function food();
}

class Animals implements Cats, Raccoon{
 public $toys;

    public function __construct( $toys){
        $this -> toys = $toys;
  
    }
 

public function food(){
    return  'Яблоки, мясо, рыба ' . $this -> toys;
}
public function color(){
    return'Животные имеют Рыжий и Черный цвет. ';
}

}

class AllAnimals{
    protected $dryfood;
    public function __construct($dryfood){
        $this -> dryfood =$dryfood;

    }
    public function getDryFood(){
return 'Все животные также получают ' . $this -> dryfood;
    }
    
}

$animals = new Animals('рацион Eнота и Кота. ');
echo $animals ->food() . $animals ->color();

$animals = new AllAnimals('Cухой корм');
echo $animals -> getDryFood();


?>









